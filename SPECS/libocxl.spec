Name:           libocxl
Version:        1.1.0
Release:        1%{?dist}
Summary:        Allows to implement a user-space driver for an OpenCAPI accelerator

License:        ASL 2.0
URL:            https://github.com/OpenCAPI/libocxl
Source0:        https://github.com/OpenCAPI/libocxl/archive/%{version}.tar.gz

ExclusiveArch:  ppc64le

BuildRequires:  gcc
BuildRequires:  doxygen

%description
Access library which allows to implement a user-space
driver for an OpenCAPI accelerator.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Recommends:     %{name}-docs

%package        docs
Summary:        Documentation files for %{name}
BuildArch:      noarch

%description    devel
The *-devel package contains header file and man pages for
developing applications that use %{name}.

%description    docs
The *-docs package contains doxygen pages for
developing applications that use %{name}.

%prep
%autosetup -p1

%build
LDFLAGS="%{build_ldflags}" CFLAGS="%{build_cflags}" make %{?_smp_mflags} V=1

%install
%make_install PREFIX=%{_prefix}

%files
%license COPYING
%doc README.md
%{_libdir}/libocxl.so.*

%files devel
%{_includedir}/*
%{_libdir}/libocxl.so
%{_mandir}/man3/*

%files docs
%{_pkgdocdir}

%changelog
* Mon Oct 08 2018 Dan Horák <dhorak@redhat.com> 1.1.0-1
- updated to 1.1.0
- Resolves: #1624645

* Thu Jun 07 2018 Dan Horák <dan[at]danny.cz> - 1.0.0-1
- updated to 1.0.0 final

* Tue Apr 10 2018 michel normand <normand@linux.vnet.ibm.com> 1.0.0-0.1
- new package and spec file of libocxl from upstream
  url: https://github.com/OpenCAPI/libocxl
